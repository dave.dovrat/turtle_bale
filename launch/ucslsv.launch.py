from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription, GroupAction, DeclareLaunchArgument
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution

from launch_ros.substitutions import FindPackageShare
from launch_ros.actions import PushRosNamespace, Node

def generate_launch_description():
	return LaunchDescription([
		DeclareLaunchArgument(
			'turtle_name',
			default_value='turtle',
			description='turtle (host) name'
		),
		DeclareLaunchArgument(
			'peer_detector_param_file',
			default_value='/home/ubuntu/turtle_bale_ws/src/turtle_bale/param/peer_detector.yaml',
			description='Full path to turtle_bale peer_detector parameter file'
		),
		DeclareLaunchArgument(
			'ucslsv_controller_param_file',
			default_value='/home/ubuntu/turtle_bale_ws/src/turtle_bale/param/ucslsv_controller.yaml',
			description='Full path to turtle_bale peer_detector parameter file'
		),
		GroupAction(
			actions=[
				# push-ros-namespace to set namespace of included nodes
				PushRosNamespace(
					LaunchConfiguration('turtle_name')
				),
				IncludeLaunchDescription(
					PythonLaunchDescriptionSource([
						PathJoinSubstitution([
							FindPackageShare('turtlebot3_bringup'),
							'launch',
							'robot.launch.py'
						])
					])
				),
				Node(
					package='turtle_bale',
					executable='peer_detector',
					parameters=[LaunchConfiguration('peer_detector_param_file')],
					name='peer_detector'
				),
				Node(
					package='turtle_bale',
					executable='ucslsv',
					parameters=[LaunchConfiguration('ucslsv_controller_param_file')],
					name='ucslsv_controller'
				),
				Node(
					package='v4l2_camera',
					executable='v4l2_camera_node',
					name='camera'
				)
			]
		)
	])
