from setuptools import setup

package_name = 'turtle_bale'

setup(
	name=package_name,
	version='2.0.0',
	packages=[package_name],
	data_files=[
		('share/ament_index/resource_index/packages',
			['resource/' + package_name]),
		('share/' + package_name, ['package.xml']),
		('share/' + package_name, ['launch/ucslsv.launch.py']),
		('share/' + package_name + '/param', ['param/peer_detector.yaml']),
		('share/' + package_name + '/param', ['param/ucslsv_controller.yaml']),
	],
	install_requires=['setuptools'],
	zip_safe=True,
	maintainer='David Dovrat',
	maintainer_email='ddovrat@cs.technion.ac.il',
	description='Helping turtles flock together since 2015',
	license='MIT',
	tests_require=['pytest'],
	entry_points={
		'console_scripts': [
			'peer_detector = turtle_bale.peer_detector:main',
			'ucslsv = turtle_bale.ucslsv:main',
		],
	},
)
