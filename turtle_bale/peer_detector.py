import rclpy
import cv2
import numpy as np

from rclpy.node import Node
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from turtle_bale_interfaces.msg import Detected

from rcl_interfaces.msg import ParameterDescriptor, ParameterEvent, SetParametersResult
from turtle_bale_interfaces.msg import Detected

class PeerDetector(Node):
	"""
	The PeerDetector detects verticle lines in a given (parameter) color in a subscribed rgb image topic
	"""

	def __init__(self):
		super().__init__('peer_detector')
		self._peerPublisher = self.create_publisher(Detected, 'detected_peers', 10)
		self._debugPublisher = self.create_publisher(Image, 'debug_image', 10)
		self._cvBridge = CvBridge()
		
		self._subscription = self.create_subscription(
			Image,
			'image_raw',
			self.imageCallback,
			10)
		self._subscription
		self.declareParameters()
		self.updateParameters()
		
		self.add_on_set_parameters_callback(self.parameterCallback)
		
################################################################################################################
		
	def declareParameters(self):
		self.declare_parameter('debug_image', True, ParameterDescriptor(description='should the node publish a debug image'))
		self.declare_parameter('detection_threshold', 50,
						 ParameterDescriptor(description='threshold for the number of pixels in the detection mask'))
		self.declare_parameter('hue_high', 255, ParameterDescriptor(description='detection maximal hue'))
		self.declare_parameter('hue_low', 100, ParameterDescriptor(description='detection minimal hue'))
		self.declare_parameter('saturation_high', 255, ParameterDescriptor(description='detection maximal saturation'))
		self.declare_parameter('saturation_low', 150, ParameterDescriptor(description='detection minimal saturation'))
		self.declare_parameter('value_high', 255, ParameterDescriptor(description='detection maximal value'))
		self.declare_parameter('value_low', 130, ParameterDescriptor(description='detection minimal value'))
		self.declare_parameter('window_columns', 640, ParameterDescriptor(description='number of columns in detection window'))
		self.declare_parameter('window_rows', 100, ParameterDescriptor(description='number of rows in detection window'))
		self.declare_parameter('window_starts_at_column', 0,
						 ParameterDescriptor(description='starting location of detection window - windows'))
		self.declare_parameter('window_starts_at_row', 90,
						 ParameterDescriptor(description='starting location of detection window - rows'))
		
################################################################################################################
		
	def updateParameters(self):
		self._debug = self.get_parameter('debug_image').value
		self._detection_threshold = self.get_parameter('detection_threshold').value
		self._hueHigh = self.get_parameter('hue_high').value
		self._hueLow = self.get_parameter('hue_low').value
		self._satHigh = self.get_parameter('saturation_high').value
		self._satLow = self.get_parameter('saturation_low').value
		self._valHigh = self.get_parameter('value_high').value
		self._valLow = self.get_parameter('value_low').value
		self._windowColumns = self.get_parameter('window_columns').value
		self._windowRows = self.get_parameter('window_rows').value
		self._windowStartsAtColumns = self.get_parameter('window_starts_at_column').value
		self._windowStartsAtRow = self.get_parameter('window_starts_at_row').value

		self._parametersChanged = False
		self.get_logger().info('Parameters Updated')
		
################################################################################################################
		
	def parameterCallback(self, event):
		self._parametersChanged = True
		return SetParametersResult(successful=True)
		
################################################################################################################
		
	def imageCallback(self, msg):
		try:
			bgrImage = self._cvBridge.imgmsg_to_cv2(msg, "bgr8")
		except CvBridgeError as e:
			self.get_logger().info("CvBridgeError: {0}".format(e))
			
		if self._parametersChanged:
			self.updateParameters()

		# crop first self._windowRows rows (out of 480), starting at self._windowStartsAtRow
		hsvImage = cv2.cvtColor(bgrImage,cv2.COLOR_BGR2HSV)[self._windowStartsAtRow : self._windowStartsAtRow + self._windowRows, self._windowStartsAtColumns : self._windowStartsAtColumns + self._windowColumns]
		
		# set HSV range
		low = np.array([self._hueLow,self._satLow, self._valLow], dtype = "uint8")
		high = np.array([self._hueHigh,self._satHigh, self._valHigh], dtype = "uint8")
		
		# mask the filtered range
		mask = cv2.inRange(hsvImage, low, high)
		
		# arbitrarily decide if there are enough edges to qualify as a peer
		msg_detected = Detected(is_detected = (cv2.countNonZero(mask) > self._detection_threshold))
		# publish the topic
		self._peerPublisher.publish(msg_detected)
		
		if self._debug:
			hsvImage = cv2.bitwise_and(hsvImage, hsvImage, mask = mask)
			bgrImage = cv2.cvtColor(hsvImage,cv2.COLOR_HSV2BGR)
			try:
				self._debugPublisher.publish(self._cvBridge.cv2_to_imgmsg(bgrImage, "bgr8"))
			except CvBridgeError as e:
				self.get_logger().info("CvBridgeError: {0}".format(e))
		
################################################################################################################
################################################################################################################

def main(args=None):
	rclpy.init(args=args)
	
	peer_detector = PeerDetector()
	
	rclpy.spin(peer_detector)
	
	peer_detector.destroy_node()
	rclpy.shutdown()

################################################################################################################
################################################################################################################

if __name__ == '__main__':
    main()
