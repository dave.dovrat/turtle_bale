import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist
from rcl_interfaces.msg import ParameterDescriptor, ParameterEvent, SetParametersResult
from turtle_bale_interfaces.msg import Detected

class Ucslsv(Node):
	"""
	UCSLSV stands for "Unicycle a(ge)nts with Crude Sensing capabilities over a Limited Sector of Visibility".
	"""
	def __init__(self):
		super().__init__('ucslsv')
		self._linear = 0.0
		self._angular = 0.0
		self._velocityCommandPublisher = self.create_publisher(Twist, 'cmd_vel', 10)
		self.declareParameters()
		self.updateParameters()
		self._subscription = self.create_subscription(
			Detected,
			'detected_peers',
			self.detectionCallback,
			10)
		self._subscription  # prevent unused variable warning
		self._timer = self.create_timer(0.1, self.timerCallback)
		self.add_on_set_parameters_callback(self.parameterCallback)
	
	def timerCallback(self):
		twist = Twist()
		twist.linear.x = self._linear
		twist.angular.z = self._angular
		self._velocityCommandPublisher.publish(twist)
	
	def declareParameters(self):
		self.declare_parameter('forward_speed', 0.21, ParameterDescriptor(description='forward speed'))
		self.declare_parameter('fast_rotation_rate', 0.5, ParameterDescriptor(description='rotation rate for no detected peers case'))
		self.declare_parameter('slow_rotation_rate', 0.25, ParameterDescriptor(description='rotation rate for detected peers case'))
	
	def updateParameters(self):
		self._linear = self.get_parameter('forward_speed').value
		self._angularFast = self.get_parameter('fast_rotation_rate').value
		self._angularSlow = self.get_parameter('slow_rotation_rate').value
		self._parametersChanged = False
		self.get_logger().info('Parameters Updated')
	
	def parameterCallback(self, event):
		self._parametersChanged = True
		return SetParametersResult(successful=True)
	
	def detectionCallback(self, msg):
		if self._parametersChanged:
			self.updateParameters()
		self._angular = self._angularFast
		if msg.is_detected:
			self._angular = self._angularSlow
	
def main(args=None):
	rclpy.init(args=args)
	ucslsv = Ucslsv()
	rclpy.spin(ucslsv)
	ucslsv.destroy_node()
	rclpy.shutdown()
	
if __name__ == '__main__':
	main()
