# turtle bale

A ROS2 turtlebot3 implementation of a simple swarming protocol using onboard sensors.

The swarming protocol is discussed in our paper ["On Gathering and Control of Unicycle A(ge)nts with Crude Sensing Capabilities"](http://doi.org/10.1109/mis.2017.4531231). 
In brief, turtles that detect other agents turn in a larger turning radius than those that do not.

## Dependencies

### exec_depend
* rclpy
* ros2launch
* python3-opencv
* cv_bridge
* python3-numpy
* sensor_msgs
* turtle_bale_interfaces [see here](https://gitlab.com/dave.dovrat/turtle_bale_interfaces)

### depend
* turtlebot3_bringup
* v4l2_camera

### test_depend
* ament_copyright
* ament_flake8
* ament_pep257
* python3-pytest

## Usage

```console
ros2 launch turtle_bale ucslsv.launch.py
```

The launch script sets up a ROS namespace under which ***turtlebot3_bringup*** is launched, along with three other nodes:

* [***ucslsv***](README.md#ucslsv) for publishing the 'cmd_vel' topic that gets the turtles moving.
* [***peer_detector***](README.md#peer_detector) for publishing the 'detected_peers' topic required by the ucslsv node.
* [***v4l2_camera_node***](https://index.ros.org/r/v4l2_camera/#foxy) for publishing the 'image_raw' topic required by the peer_detector node.

## Launch Arguments

The launch script *ucslsv.launch.py* accepts the following arguments:


| Argument 	| Meaning				| Default Value	| Remark	|
| ---------	| ------------			| -------			| -------	|
| turtle_name	| The turtle's namespace	| turtle			| a unique namespace is important otherwise the robots interfere with eachother's topics	|
| peer_detector_param_file	| Where the *peer_detector* node's parameter file can be found	| a reasonable location on the turtlebot3 filesystem	| see [peer_detector](README.md#peer_detector_params) parameters for more details.	|
| ucslsv_controller_param_file	| Where the *ucslsv* node's parameter file can be found	| a reasonable location on the turtlebot3 filesystem	| see [ucslsv](README.md#ucslsv_params) parameters for more details.	|

## <a name="ucslsv"></a>ucslsv

UCSLSV stands for *Unicycle agents with Crude Sensing over a Limited Sector of Visibility*, and is the swarming algorithm implemented in this node.

### <a name="ucslsv_params"></a>ucslsv Parameters

The ucslsv node publishes a *Twist* type message on the *cmd_vel* topic.
The *twist.linear.x* value is kept constant at *forward_speed*, while *twist.angular.z* alternates according to the algorithm's input between *fast_rotation_rate* and *slow_rotation_rate*.

| Parameter 	| Meaning		| Default Value	| Remark	|
| ---------	| ------------	| -------		| -------	|
| fast_rotation_rate	| The angular velocity around the z axis in the robot's frame in case no peers are detected	| 0.5	| Approx Rad/Sec.	|
| forward_speed	| The linear velocity aligned with the robot's x axis in the robot's frame	| 0.21	| Approx m/Sec.	|
| slow_rotation_rate	| The angular velocity around the z axis in the robot's frame in case peers are detected	| 0.25	| Approx Rad/Sec.	|
| use_sim_time	| Should the time also be simulated	| false	| See [ROS Clock](http://wiki.ros.org/Clock)	|

## peer_detector

The peer_detector node detects turtle peers.
While explaining what a turtlebot looks like is kinda hard when explaining to a fellow human, and describing a turtlebot using computer vision is even harder, describing a pole in a specific hue is rather easy for both humans and machines, so that's what we did here.
We fitted the robots with nice pink poles.
Now, incoming images are cropped to little windows where we search for pink pixels. If we find more than a certain threshold of pixels in the right color, that means that there is a peer (actually a pole bearing turtle) in our visibility sector.

### <a name="peer_detector_params"></a>peer_detector Parameters

Most peer_detector parameters revolve around the HSV values and size of the visibility sector window.

| Parameter 	| Meaning		| Default Value	| Remark	|
| ---------	| ------------	| -------		| -------	|
| debug_image	| Should the node publish its detected pixels overlaid over the raw incoming image on the *debug_image* topic?	| false	| It's cool to watch and helpful when trying to figure out what HSV values work for your colorful poles, but leave it false when deploying multiple robots or you'll choke your network.	|
| detection_threshold	| How many pixels in the right color mean that a peer is detected?	| 50	| You might want to change this value depending on your image resolution.	|
| hue_high	| upper H in HSV	| 255 | pink.	|
| hue_low	| lower H in HSV	| 100 | pink.	|
| saturation_high	| upper S in HSV	| 255 | pink.	|
| saturation_low	| lower S in HSV	| 150 | pink.	|
| value_high	| upper V in HSV	| 255 | pink.	|
| value_low	| lower V in HSV	| 130 | pink.	|
| window_columns	| How many columns in the search window?	| 640	| You might want to change this value depending on your image resolution.	|
| window_rows	| How many rows in the search window?	| 100	| You might want to change this value depending on your image resolution.	|
| window_starts_at_column	| Where to start the search window	| 0	| -	|
| window_starts_at_row	| Where to start the search window	| 90	| We decided the ceiling wasn't very interesting.	|
| use_sim_time	| Should the time also be simulated	| false	|	See [ROS Clock](http://wiki.ros.org/Clock)	|
